# Animal Classifier AI

Austin Agronick

This was developed as a capstone project for my study at the University of Rhode Island under the guidance of Dr. Lutz Hamel.
 
For my independent research project, I implemented a decision tree learning algorithm. Given any set of training data in .csv format with newline separated instances and comma separated attributes, it will approximate a learned target function, which is represented by a decision tree. This decision tree implementation uses Python 3, pandas library, and NumPy library. The get_data() function uses the pandas library to read a .csv to a dataframe. The entropy() function uses a formula detailed in Chapter 3 of Tom M. Mitchell’s book Machine Learning on pages 56 and 57 to measure the entropy value of a given attribute. The entropy is a measure of the impurity of a collection of training examples and is used to calculate the information gain. The info_gain() function calculates the information gain which is a measure of the expected reduction in entropy caused by partitioning the examples in a decision tree by a particular attribute. This formula is detailed on page 58 of Mitchell’s book. The build_tree() function is a recursive implementation of the ID3 algorithm that builds a decision learning tree. The tree is grown until it has been split among attributes from most to least information gain, until all training instances’ target attribute can be uniquely identified using the tree. The search_tree() function is used to classify new unseen instances using the trained and built decision tree. If this function encounters an attribute that is unknown to the learning tree, it will default to the most commonly occurring target value. To test the algorithm, I used a dataset (https://archive.ics.uci.edu/ml/datasets/zoo) of zoo animals to classify unknown animals into 1 of 7 animal types. Here are the classifications of animals provided by the dataset:
           1 (41) aardvark, antelope, bear, boar, buffalo, calf,
                  cavy, cheetah, deer, dolphin, elephant,
                  fruitbat, giraffe, girl, goat, gorilla, hamster,
                  hare, leopard, lion, lynx, mink, mole, mongoose,
                  opossum, oryx, platypus, polecat, pony,
                  porpoise, puma, pussycat, raccoon, reindeer,
                  seal, sealion, squirrel, vampire, vole, wallaby,wolf
           2 (20) chicken, crow, dove, duck, flamingo, gull, hawk,
                  kiwi, lark, ostrich, parakeet, penguin, pheasant,
                  rhea, skimmer, skua, sparrow, swan, vulture, wren
           3 (5)  pitviper, seasnake, slowworm, tortoise, tuatara
           4 (13) bass, carp, catfish, chub, dogfish, haddock,
                  herring, pike, piranha, seahorse, sole, stingray, tuna
           5 (4)  frog, frog, newt, toad
           6 (8)  flea, gnat, honeybee, housefly, ladybird, moth, termite, wasp
           7 (10) clam, crab, crayfish, lobster, octopus, scorpion, seawasp, slug, starfish, worm

My program removes 5 random animals from the dataset to test on each execution. After the random test instances are removed from the dataset the tree is trained without them. I have found my implementation of the algorithm to be about 80-90% accurate at identifying animals into the correct type. Such margin of error is expected as I have not implemented any of the improvements discussed in Mitchell’s book such as pruning, more unbiased attribute selection algorithms, using attribute selection measures other than information gain, or considering costs associated with instance attributes. However, this implementation can successfully classify this zoo dataset composed of boolean as well as numerical attributes considerably well without these extensions to the basic ID3 algorithm.

