from pprint import pprint
import pandas
import numpy

# Austin Agronick
# Decision Tree Learning Project 12/16/19


# given a dataset file,
# returns a dataset stored in a pandas DataFrame.
def get_data(file):
    data = pandas.read_csv(file, delimiter=',',
                           names=['animal', 'hair', 'feathers', 'eggs', 'milk', 'airborne', 'aquatic', 'predator',
                                  'toothed', 'backbone', 'breathes', 'venomous', 'fins', 'legs', 'tail', 'domestic',
                                  'catsize', 'type'])

    return data


# given a dataset attribute,
# returns the entropy value of the attribute.
# entropy is used to calculate the information gain of an attribute.
def entropy(attribute):
    collection, counts = numpy.unique(attribute, return_counts=True)
    entropy = numpy.sum(
        [numpy.log2(counts[i] / numpy.sum(counts)) * (-counts[i] / numpy.sum(counts)) for i in range(len(collection))])

    return entropy


# given a dataset to calculate information gain for, a target attribute, and an attribute for which to calculate the information gain for,
# returns the information gain of the dataset for the specified attribute.
# information gain is used to select the attribute that reduces entropy the most, to split the dataset during tree building.
def info_gain(dataset, target_attribute, info_attribute="default"):
    # entropy of entire dataset
    total_entropy = entropy(dataset[info_attribute])
    # calculate values and counts for target attribute
    values, counts = numpy.unique(dataset[target_attribute], return_counts=True)

    # calculate weighted entropy
    attribute_entropy = numpy.sum(
        [(counts[i] / numpy.sum(counts)) * entropy(
            dataset.where(dataset[target_attribute] == values[i]).dropna()[info_attribute]) for i in range(len(values))])

    # return information gain
    return total_entropy - attribute_entropy


# given a dataset to create a decision tree or the remainder of a dataset for a recursively growing decision tree,
    # the initial dataset to calculate the classification value for a target attribute of the initial dataset,
    # the target attribute to grow the next node with,  and a list of all attributes in the original dataset since
    # attributes will be lost after recursive calls when splitting for each new node,
# returns a decision tree as a python dictionary
# this function is essential to build the decision tree which is used to make predictions for a new dataset.
def build_tree(dataset, initial_dataset, attributes, target_attribute, parent_class=None):
    # return a leaf node if a condition is true

    # if all target attributes are the same, return the repeating value
    if len(numpy.unique(dataset[target_attribute])) <= 1:
        return numpy.unique(dataset[target_attribute])[0]

    # if there are no attributes, return the target attribute of immediate parent node
    elif len(attributes) == 0:
        return parent_class

    # if empty dataset, return target attribute value from initial dataset
    elif dataset.empty:
        return numpy.unique(initial_dataset[target_attribute])[
            numpy.argmax(numpy.unique(initial_dataset[target_attribute], return_counts=True)[1])]

    # grow tree
    else:
        # set the default value for this node, as target attribute value of the current node
        parent_class = numpy.unique(dataset[target_attribute])[
            numpy.argmax(numpy.unique(dataset[target_attribute], return_counts=True)[1])]

        # select the attribute which best splits the dataset
        item_values = []
        for attribute in attributes:  # get information gain values for the features
            item_values.append(info_gain(dataset, attribute, target_attribute))
        max_gain_attribute_index = numpy.argmax(item_values)
        max_gain_attribute = attributes[max_gain_attribute_index]

        # build tree structure with the root as the max_gain_attribute
        tree = {max_gain_attribute: {}}

        # remove attribute with most information gain
        attributes = [a for a in attributes if a != max_gain_attribute]

        # grow a child branch for every possible value of the root node attribute
        for value in numpy.unique(dataset[max_gain_attribute]):
            # split dataset along attribute with largest information gain
            sub_tree = dataset.where(dataset[max_gain_attribute] == value).dropna()
            # run ID3 algorithm recursively for collection of each sub_tree data
            subtree = build_tree(sub_tree, dataset, attributes, target_attribute, parent_class)
            # add built sub tree to the tree under the root node
            tree[max_gain_attribute][value] = subtree

        return tree


# given an instance and its attributes to predict a classification for, and an ID3 tree built from a dataset file,
# returns a classification prediction
# this provides purpose to the search tree algorithm, allowing the trained tree to make
# predictions given new queries.
def search_tree(instance, tree, most_common_target_value):
    # for each instance attribute in the tree
    for attribute in instance:
        if attribute in tree:
            # if attribute is unknown to tree assume the most common classification
            if instance[attribute] not in tree[attribute]:
                return most_common_target_value

            # get the node value for this attribute
            classification = tree[attribute][instance[attribute]]
            # if not a leaf node continue search recursively
            if isinstance(classification, dict):
                return search_tree(instance, classification, most_common_target_value)
            else:
                return classification

# split training data, relabeling indices to start from 0
def train_test_split(dataset):
    testing_data = dataset.iloc[80:].reset_index(drop=True)
    training_data = dataset.iloc[:80].reset_index(drop=True)

    return [testing_data, training_data]


# prepare dataset
dataset = get_data('zoo.csv')
animal_list = dataset[['animal']].copy()
dataset = dataset.drop('animal', axis=1)
target_attribute = 'type'

# remove 5 instances for testing
tests = []
for i in range(5):
    num = numpy.random.randint(0, 100)
    tests.append([num, dataset.iloc[num].to_dict()])
    dataset = dataset.drop(dataset.index[num])

# train dataset and build tree
testing_data, training_data = train_test_split(dataset)
tree = build_tree(training_data, training_data, training_data.columns[:-1], target_attribute)

pprint(tree)
print('\n')
#pandas.set_option('display.max_rows', None)
#print(dataset)
#pprint(animal_list)

# get most common attribute to classify unknown attributes and test targets using tree
most_common_target_value = dataset[target_attribute].mode().iloc[0]
for test in tests:
    print(animal_list.iloc[test[0]][0], 'is detected as type', int(search_tree(test[1], tree, most_common_target_value)))


